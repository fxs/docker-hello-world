# jfxs / hello world

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/hello-world/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/hello-world/pipelines)

A [Nginx](https://hub.docker.com/_/nginx) Docker image:

* **lightweight** image based on Alpine Linux only 5 MB,
* multiarch with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing SBOM changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* an **SBOM attestation** added using [Syft](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

![Hello world screenshot](https://gitlab.com/op_so/docker/hello-world/-/raw/master/hello.png)

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/hello-world) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/hello-world) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/hello-world) The Quay.io registry.

## Running Nginx Hello World

```shell
docker run -d --rm -p 8080:8080 jfxs/hello-world
```

or

```shell
docker run -d --rm -p 8080:8080 quay.io/ifxs/hello-world
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/hello-world/-/blob/main/Dockerfile) and contains:

--SBOM-TABLE--

Details are updated on [Dockerhub Overview page](https://hub.docker.com/r/jfxs/hello-world) when an image is published.

## Versioning

The Docker tag is defined by the nginx version used and an increment to differentiate build with the same nginx version:

```text
<nginx_version>-<increment>
```

Example: 1.18.0-001

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
