# hadolint ignore=DL3007
FROM nginxinc/nginx-unprivileged:alpine-slim

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="hello-world" \
    org.opencontainers.image.description="A lightweight automatically updated and tested Hello world Docker image" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/hello-world" \
    org.opencontainers.image.source="https://gitlab.com/fxs/hello-world" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files/default.conf /etc/nginx/conf.d/default.conf
COPY files/index.html /usr/share/nginx/html/index.html

HEALTHCHECK --interval=1m --timeout=3s \
  CMD wget -O /dev/null http://localhost:8080 || exit 1
